game={};

game.canvas= document.createElement('canvas');
game.canvas.width= 900;
game.canvas.height=600;
game.lastrender= 0;
game.score= 0;
game.scorePlayerOne=0;
game.scorePlayerTwo=0;
game.end= false;
game.ctx= game.canvas.getContext('2d');
document.body.appendChild(game.canvas);
game.states={win:false,start:true, play:false};
game.scene=function(scene){
    scene.update();
    scene.draw();

};
game.scene.current="";
game.changescene= function(scene)
{
    game.ctx.clearRect(0,0,game.canvas.width,game.canvas.height);
    game.scene.current={name:scene.name};
    game.scene(scene);

};
game.gameover= {};
game.gameover.name="gameover";
game.gameover.draw=function()
{
game.drawObjects('rect', "#OOOOOO",{x:0, y:0, width:game.canvas.width, height: game.canvas.height});
    game.drawText(game.canvas.width/3,game.canvas.height/3,"40px Arial","#ffffff","Game Over");
    game.drawText(game.canvas.width/3,(game.canvas.height/3)+40,"20px Arial","#ffffff","Press enter to play again");

};
game.gameover.update= function()
{

};

game.level={};
game.level.name="level";

game.level.update=function(){
PlayerOne.update();
PlayerTwo.update();
ball.update();


};

game.level.draw= function()
{
    game.ctx.clearRect(0,0,game.canvas.width,game.canvas.height);
    ball.draw();
    PlayerOne.draw();
    PlayerTwo.draw();
    block.draw();
    game.drawText(8,20, "20px Arial","#000000","Gracz Pierwszy: "+ game.scorePlayerOne);
    game.drawText(game.canvas.width-200,20, "20px Arial","#000000","Gracz Drugi: "+ game.scorePlayerTwo);
    game.ctx.lineWidth=3;
    game.ctx.strokeStyle='#1C2378';
    game.ctx.strokeRect(0,0,game.canvas.width, game.canvas.height);

};
game.startgame={};
game.startgame.name="start";
game.startgame.update= function()
{

};
game.startgame.draw=function()
{
game.drawObjects('rect', "#0065ff",{x:0, y:0, width:game.canvas.width, height: game.canvas.height});
  game.drawText(game.canvas.width/3,game.canvas.height/2, "50px Arial","#ffffff","Valley");
    game.drawText(game.canvas.width/3,(game.canvas.height/2)+20, "20px Arial","#ffffff","Press Enter to start game");
};

game.win={};
game.win.name="win";
game.win.update= function(){};
game.win.draw=function()
{
  game.drawObjects('rect', "#78761F",{x:0, y:0, width:game.canvas.width, height: game.canvas.height});
  game.drawText(game.canvas.width/3,game.canvas.height/2, "50px Arial","#ffffff","You win!!!");
};
game.drawObjects= function(object,color, coords)
{
    game.ctx.beginPath();
    if(object==='rect')
    {
        game.ctx.rect(coords.x,coords.y, coords.width, coords.height)
    }
    else if(object==='ball')
    {
        game.ctx.arc(coords.x, coords.y,coords.radius,0,Math.PI*2);
    }
    game.ctx.fillStyle=color;
    game.ctx.fill();
    game.ctx.closePath();

};
game.drawText= function(x,y, font, color, text)
{
    game.ctx.font= font;
    game.ctx.fillStyle= color;
    game.ctx.fillText(text, x,y);
};



game.main= function()
{
    if(game.states.start)
    {
    game.changescene(game.startgame);
  }
    if(game.states.play)
    {
      game.changescene(game.level);
    }
    if(game.states.end)
    {
        game.changescene(game.gameover);
    }
    if(game.states.win)
    {
        game.changescene(game.win);
    }

    requestAnimationFrame(game.main);
};

requestAnimationFrame(game.main);
