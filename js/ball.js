ball={};
ball.x=50;
ball.y=200;
ball.radius=10;
ball.dx=2;
ball.dy= -2;
ball.vx=(Math.random() * -2) - 1;
ball.vy=(Math.random() * -3) - 1;
ball.gravity=0.05;
ball.color= '#00f';
ball.draw= function()
{
    game.drawObjects('ball',ball.color, {x: ball.x, y:ball.y, radius: ball.radius});


};
ball.update= function()
{
    if(ball.x +ball.vx> game.canvas.width- ball.radius ||ball.x +ball.vx <ball.radius)
    {
        ball.vx= -ball.vx;
    }
    if(ball.y + ball.vy< ball.radius)
    {

        ball.vy = - ball.vy;
    }
    else if(ball.y + ball.vy> game.canvas.height - ball.radius )
    {

        ball.vy = - ball.vy;
        if(ball.collision(PlayerOne)||ball.collision(PlayerTwo)){
            ball.vy = - ball.vy;
        }

        if(ball.collision(PlayerOne)===false)
        {

            if(ball.x< game.canvas.width/2 && ball.x>0)
            {
                game.scorePlayerOne++;
            }
        }
         if(ball.collision(PlayerTwo)===false)
        {
            if(ball.x> game.canvas.width/2 && ball.collision(PlayerTwo)===false)
            {
                game.scorePlayerTwo++;
            }
        }




    }
    if(ball.collision(block)===true)
    {
        ball.vy=+ ball.vy;
        ball.vx=- ball.vx;
    }

    ball.vy+= ball.gravity;
    ball.x+=ball.vx;
    ball.y+=ball.vy;



};
ball.collision= function(rect)
{
    var distx= Math.abs(ball.x - rect.x- rect.width/2);
    var disty= Math.abs(ball.y - rect.y- rect.height/2);
    if(distx >(rect.width/2 + ball.radius)){return false}
    if(disty >(rect.height/2 + ball.radius)){return false}
    if(distx<=(rect.width/2)){return true;}
    if(distx<=(rect.height/2)){return true;}
    var dx=distx-rect.width/2;
    var dy= disty-rect.height/2;
    return (dx*dx+dy*dy<=(ball.radius*ball.radius));

};
